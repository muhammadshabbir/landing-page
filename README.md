# Tutor Landing Page

## Description

Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.
This is a landing page for a tutor website - that I have made up. Check it out here https://muhammadshabbir.gitlab.io/landing-page/
It is not fully functional - my main aim of this project was to make it look aesthetically pleasing.

## Issues

I realise that this is mainly compatible for desktop users. This is something I will take into account when planning and creating future projects.

## Authors and acknowledgment

Credit for images
 - Katerina Holmes (Pexels) for the whiteboard image and first picture in about section.
 - Cottonbro Studio (Pexels) for second picture in about section.
 - ThisIsEngineering (Pexels) for third picture in about section.
 - Mati Mango (Pexels) for fourth picture in about section.	
 - LinkedIn, Gmail(icons8) and GitLab for their images.
 - icons8 for favicon
## License

All rights are reserved.
